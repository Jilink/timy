@echo off 

  IF %1 == nocode (
    cd "pages"
    start chrome home.html

  ) ELSE IF %1 == code (
    code .
  ) ELSE (
    code . 
    cd "pages"
    start chrome home.html
  )
